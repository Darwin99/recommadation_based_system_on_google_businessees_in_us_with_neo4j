# [Recommendation based system on google businessees in US (Case of Wyoming)](https://gitlab.com/Darwin99/recommadation_based_system_on_google_businessees_in_us_with_neo4j/-/tree/main?ref_type=heads)


## Auteurs
- [DARIL RAOUL KENGNE WAMBO](  <a href="mailto:kengne_wambo.daril_raoul@courrier.uqam.ca">**```KEND82330000```**</a> )
- [TINHINANE BOUDIAB](  <a href="mailto:boudiab.tinhinane@courrier.uqam.ca">**```BOUT79360000```**</a> )
- [YVES FERSTLER](  <a href="mailto:ferstler.yves@courrier.uqam.ca">**```  FERY72290101```**</a> )
- [OHAN SAMANTHA SONFACK ANEWOU](  <a href="mailto:sonfack_anewou.johan_samantha@courrier.uqam.ca">**```SONJ86350009```**</a> )

Retrouvez le code de ce projet sur [gitLab](https://gitlab.com/Darwin99/recommadation_based_system_on_google_businessees_in_us_with_neo4j/-/tree/main?ref_type=heads). 



Ce système de recommandation est basé sur les données des businesses de google dans l'état de Wyoming. **`Il permet de recommander à un ou plusieurs business les aspects à améliorer dans leur service en se basant sur les reviews des autres utilisateurs dans des businesses similaires`** (`même catégorie`).
Un exemple basique est un restaurant qui veut améliorer son service. Il peut utiliser ce système de recommandation pour savoir les aspects à améliorer en se basant sur les reviews des autres restaurants dans la même catégorie. En l'occurrence,les commentaires des utilisateurs qui ont apprecié l'acceuil, la qualité de la nourriture, le prix, dans d'autres restaurants mieux noté de la même catégorie. Et ainsi, le restaurant en question pourra travailler sur ces aspects pour améliorer son service.

Également, les businesses desquels on récupère les reviews sont ceux
- qui ont un `nombre de reviews considérable` (En utilisant un seuil arbitraire)
- qui sont mieux notés, c'est à dire qui ont une `moyenne de rating supérieure` à celle du business à qui on recommande les aspects à améliorer (En utilisant un seuil arbitraire)
- qui ont une `similarité de Jaccard supérieure à 0.5` avec le business à qui on recommande les aspects à améliorer (Toujours en utilisant un seuil arbitraire). Dans ce cas, ceci nous permettra de choisir les businesses les plus similaires au business à qui la recommandation est faite pour question de plus de pertinence. 
## Prétraitement des données
Pour un question de lisibilité du notebook python dans [preprocessing.ipynb](preprocessing.ipynb) qui a servi de prétraitement, nous recommandons de l'ouvrier dans le web via ce lien [https://gitlab.com/Darwin99/recommadation_based_system_on_google_businessees_in_us_with_neo4j/-/blob/main/preprocessing.ipynb?ref_type=heads](https://gitlab.com/Darwin99/recommadation_based_system_on_google_businessees_in_us_with_neo4j/-/blob/main/preprocessing.ipynb?ref_type=heads)


## [IMPORTATION DES DONNEES DANS NEO4J](#importation-des-données-dans-neo4j)

### Configuration de l'importation des données de type json dans neo4j

Pour importer des données de type json dans neo4j, nous avons utilisé le plugin `APOC` qui permet de charger des données json dans neo4j. Pour cela, nous avons suivi les étapes suivantes:

#### 1. Definition du repertoire plugins et activation du plugin APOC

- Cliquer sur `les trois points` à droite de la base de données une fois celle-ci créée et cliquer sur `terminal` comme dans l'image ci-dessous
![settings_image](images/upload-json/settings.png)
- Naviguer dans le dossier `conf` puis créer  le fichier `apoc.conf` dans lequel il faut ajouter la ligne suivante `apoc.import.file.enabled=true`.

- Ensuite, localiser le repertoire `plugins` et taper la commande `pwd` dans le terminal et copier le chemin du repertoire.
- Revenir dans neo4j et cliquer sur `settings` comme dans [cette image](images/upload-json/settings.png)  [settings_image]: puis ajouter la ligne suivante `dbms.directories.plugins=chemin_du_repertoire_plugins`. Attention, si vous avez des espaces dans le chemin, il faut les remplacer par `\ ` et si vous êtes sur windows, il faut remplacer les `\` par `/` dans le chemin.


#### 2. Telechargement du plugin APOC


- Selectionner la base de donnéed dans neo4j puis dans le panel de droite, cliquer sur `Plugins` puis sur `Plugins` comme dans [cette image](images/upload-json/apoc_installation.png) [manage_image]:
- Cliquer sur `install` pour installer le plugin APOC comme dans l'image ci-dessous ![cette image](images/upload-json/apoc_installation.png).


## IMPORTATION DES DONNEES DANS NEO4J
Il faut noter que nous n'avons utilisé qu'une partie des données car, celles-ci prenant beaucoup de temps pour le chargement en raison de la taille du jeu de données. Nous avons donc utilisé les données des businesses de l'état de Wyoming(elles même réduites). Pour cela, nous avons utilisé les fichiers `meta-Wyoming.json` et `review_wyoming_dataset_clean.json` qui sont disponibles [ici](https://drive.google.com/drive/folders/15ZTq1T5GBaAuCajcv4VceCwS4pIctV-l?usp=sharing)

Dans la suite, étant donné la taille du jeu données importante ne pouvant être chargée en un temps relativement court et pour faciliter les tests, nous vous proposons d'utiliser les fichiers(  [review_dataset_test.json](review_dataset_test.json) et[meta_data_test.json](meta_data_test.json) ) qui est sont tout simplement des extraits des fichiers pretraités disponibles [ici](https://drive.google.com/drive/folders/15ZTq1T5GBaAuCajcv4VceCwS4pIctV-l?usp=sharing) dans lequel nous pouvons choisir alátoirement le business de gmap_id `0x876f3a50f1b7f3c7:0x4846fcc0819f1572` qui correspond au business `Colbert Kirk DDS` dans l'état de Wyoming. 


### 1. IMPORTATION DES BUSINESSES DANS NEO4J

D'après le prétraitement des données, pour un business, nous avons retenue les informations suivantes: name, gmap_id, latitude, longitude, avg_rating, num_of_reviews, hours, url, relative_results, category. Nous allons importer ces données dans neo4j en créant les noeuds `Business` et `Category` et les relations `IS_IN` et `RELATIVE` entre les noeuds `Business`.
**Assurez vous d'avoir le fichier `recommadation_based_system_on_google_businessees_in_US/meta-Wyoming.json` dans votre dossier d'imports de neo4j ou de la base de données**

  
```cypher
WITH "recommadation_based_system_on_google_businessees_in_US/meta_data_test.json" AS url
// Recuperation les donnees du fichier json dans la variable value
CALL apoc.load.json(url) YIELD value 
WITH value
// Creation d'un noeud "Category" avec les valeurs present dans la clef "category" du fichier json
UNWIND value.category AS category
MERGE (c:Category { name: category })

// Transformation des données "heures" (hours) en chaîne de caractères car elle permettrait au business de consulter les heures d'ouverture de l'autre business
WITH value, c,
(
CASE
 WHEN value.hours is null or size(value.hours) =0  then ""
else  value.hours 
end
) AS hours

// Tranformation des données "entreprises relatives recommandée" (relative_results) en chaîne de caractères
WITH value, c, hours,
(
CASE
 WHEN value.relative_results is null  or size(value.relative_results) =0  then ""
else value.relative_results
end
) AS relative_results

// Creation d'un noeud "Business" contenant les valeurs du fichier json, à l'exception de 'category'
MERGE (b:Business { name: value.name, gmap_id: value.gmap_id, latitude: value.latitude, longitude: value.longitude, avg_rating: value.avg_rating, num_of_reviews: value.num_of_reviews, hours: hours, url: value.url, relative_results: relative_results })
// Creation d'une relation "IS_IN" entre les Business et les Categories
MERGE (b)-[:IS_IN]->(c)
WITH b, value WHERE value.relative_results is not null  
// Ajout de la relation relative entre les businesses
UNWIND value.relative_results AS relative_result
MATCH (b1:Business { gmap_id: value.gmap_id })
MATCH (b2:Business { gmap_id: relative_result })
MERGE (b1)-[:RELATIVE]->(b2)
RETURN b



```

Nous pouvons voir quelques résultats dans la capture suivante: 

![businesses](images/businesses.png)



### 2. IMPORTATION DES REVIEWS DANS NEO4J

D'après le prétraitement des données, pour une review, nous avons les informations suivantes: user_id, gmap_id, rating, text. Nous allons importer ces données dans neo4j en créant les noeuds `User` et `Business` et la relation `REVIEWED` entre les noeuds `User` et `Business`. Ceci car un User peut avoir plusieurs reviews et un business peut avoir plusieurs reviews.

**Assurez vous également d'avoir le fichier `recommadation_based_system_on_google_businessees_in_US/review_wyoming_dataset_clean.json` dans votre dossier d'imports de neo4j ou de la base de données**



Nous avons donc obtenu les résultats suivants:
```cypher
WITH "recommadation_based_system_on_google_businessees_in_US/review_wyoming_dataset_clean.json" AS url
CALL apoc.load.json(url) YIELD value 
MERGE (u:User { user_id: value.user_id })
WITH u, value
// Création d'une relation "REVIEWED" entre "User" et "Business", cette relation montre quel utilisateur a évalué quel business
MATCH (b:Business { gmap_id: value.gmap_id })
MERGE (u) -[:REVIEWED{ rating: value.rating, text: value.text}]->(b)
```
On peut voir dans les deux captures suivants suivantes qui montrent les noeuds `User` et `Business` et la relation `REVIEWED` entre les noeuds `User` et `Business`:
![users](images/reviews.png)
![reviews](images/details_reviews.png)


## RECOMMANDATION DES ASPECTS A AMELIORER  DANS UN BUSINESS AVEC NEO4J
Pour le calcul de la similarite entre les businesses, nous allons utiliser l'indice de Jaccard relativement aux categories des businesses. 
Nous nous somme basé sur l'utilisation de [appoc union](https://neo4j.com/labs/apoc/4.3/overview/apoc.coll/apoc.coll.union/) et des sous-requêtes [`CALL`](https://neo4j.com/docs/cypher-manual/current/subqueries/call-subquery/)s



Dans le but de tester et d'eviter la complexité du code, nous allons tout d'abord pr'esenter les sous-requêtes.


### SOUS-REQUETE 1: RECUPERATION DES BUSINESSES AVEC LESQUELS ON CALCULE LA SIMILARITE

```cypher
MATCH (b1:Business{gmap_id: "0x876f3a50f1b7f3c7:0x4846fcc0819f1572"})-[:IS_IN]->(c:Category)<-[:IS_IN]-(b2:Business)
WHERE b1.gmap_id<>b2.gmap_id //
WITH b1, b2, count(c) AS intersection_size
WHERE intersection_size > 0 //Pour eviter une division par 0
MATCH (b1)-[:IS_IN]->(c1:Category)  
WITH b1, b2, intersection_size, collect(c1) AS b1_categories
MATCH (b2)-[:IS_IN]->(c2:Category)
WITH b1, b2, intersection_size, b1_categories, collect(c2) AS b2_categories
WITH b1, b2, intersection_size, apoc.coll.union(b1_categories, b2_categories) AS union_categories

//Construction de l'union
WITH b1, b2, (intersection_size)/(size(union_categories)) as jacard_similarity
WHERE jacard_similarity >0 AND b2.num_of_reviews >=0 AND b2.avg_rating > b1.avg_rating //0.5 et 10 peuvent etre des seuils abitraires
RETURN   b2 AS best_b1_busisiness
ORDER BY b2.num_of_reviews DESC

```

![similar_businesses](images/similarities.png)

Dans l'image ci-dessus, en partie droite nous avons les résultats des business similiarire au business de gmap_id `0x53345db0e11e1b8d:0x65c1bfe3612e74a4` aléatoirement choisit et avec des contraintes sur le nombre de reviews et la moyenne de rating(Pour rendre plus pertinenent les business sur lesquels le business en besoin d'amélioration peut considérer). En partie gauche, nous avons la categorie des business similaires et on peut coir qu'ils sont liés au business de gmap_id `0x53345db0e11e1b8d:0x65c1bfe3612e74a4` par la relation `IS_IN`. Et certains entre eux sont liés par la relation `RELATIVE` qui montre que ces business sont similaires.

**NB: Il faut noter que les seuil de limites de similarité et du nombre de reviews spécifiés ici sont juste arbitraire et ainsi nous pouvons être plus ou moins strictes dans les résultats en les faisant varier** 

### SOUS-REQUETE 2: RECUPERATION DES REVIEWS DES BUSINESS SIMILAIRES

Une fois les business similaires obtenus, nous allons récupérer les reviews de ces business similaires. Ceci nous permettra de savoir les aspects à améliorer dans le business à qui la recommandation est faite. Nous faisons suivre la requête précédente par la requête suivante:



```cypher
WITH best_b1_busisiness
MATCH  (:User) -[r:REVIEWED]->(best_b1_busisiness)
WHERE r.text is not null and size(r.text) > 0 and r.rating is not null and r.rating > 3.5 //seuils arbitraires pouvant être modifiés
RETURN r.text,best_b1_busisiness.url, best_b1_busisiness.hours,best_b1_busisiness.relative_results
```


### REQUETE COMPLETTE


```cypher
CALL {MATCH (b1:Business{gmap_id: "0x876f3a50f1b7f3c7:0x4846fcc0819f1572"})-[:IS_IN]->(c:Category)<-[:IS_IN]-(b2:Business)
WHERE b1.gmap_id<>b2.gmap_id //
WITH b1, b2, count(c) AS intersection_size
WHERE intersection_size > 0 //Pour eviter une division par 0
MATCH (b1)-[:IS_IN]->(c1:Category)  
WITH b1, b2, intersection_size, collect(c1) AS b1_categories
MATCH (b2)-[:IS_IN]->(c2:Category)
WITH b1, b2, intersection_size, b1_categories, collect(c2) AS b2_categories
WITH b1, b2, intersection_size, apoc.coll.union(b1_categories, b2_categories) AS union_categories

//Construction de l'union
WITH b1, b2, (intersection_size)/(size(union_categories)) as jacard_similarity
WHERE jacard_similarity >0.5 AND b2.num_of_reviews >=10 AND b2.avg_rating > b1.avg_rating //0.5 et 10 sont des seuils abitraires
RETURN   b2 AS best_b1_busisiness
ORDER BY b2.num_of_reviews DESC}

WITH best_b1_busisiness
MATCH  (:User) -[r:REVIEWED]->(best_b1_busisiness)
WHERE r.text is not null and size(r.text) > 0 and r.rating is not null and r.rating > 3.5 //seuil arbitaire
RETURN r.rating ,r.text,best_b1_busisiness.url, best_b1_busisiness.hours,best_b1_busisiness.relative_results
```



Les recommandations pour ce business pourraient être d'ameliorer son service et de se rendre plus disponible pour les clients. En effet, les reviews des autres utilisateurs montrent que le business est bien noté pour la qualité de son service et que le business est toujours disponible pour les clients.

Et donc ces aspects pourraient être améliorés en se basant sur les reviews des autres utilisateurs dans les business similaires.

![recommandations](images/recommadations.png)

**A noter que pour trouver des recommandations d'autres buisness, il suffit de remplacer leur gmap_id dans la requête complète. Et on pourrait avoir une requete complexe qui fait le calcul pour chaque business**


A cette requête, nous pouvons ajouter la contrainte de `trier par business les plus proches` en calculant la distance entre les business similaires et le business à qui la recommandation est faite. Car il est possible que les souccis que rencontre le business en besoin puisse être résolus par les commentaires positifs des business les plus proches donc avec qui ils partage la **même clientèle** Pour cela, nous allons utiliser la fonction `point.distance` de neo4j. Nous allons donc modifier la requête précédente comme suit:

```cypher
CALL {MATCH (b1:Business{gmap_id: "0x876f3a50f1b7f3c7:0x4846fcc0819f1572"})-[:IS_IN]->(c:Category)<-[:IS_IN]-(b2:Business)
WHERE b1.gmap_id<>b2.gmap_id //
WITH b1, b2, count(c) AS intersection_size
WHERE intersection_size > 0 //Pour eviter une division par 0
MATCH (b1)-[:IS_IN]->(c1:Category)  
WITH b1, b2, intersection_size, collect(c1) AS b1_categories
MATCH (b2)-[:IS_IN]->(c2:Category)
WITH b1, b2, intersection_size, b1_categories, collect(c2) AS b2_categories
WITH b1, b2, intersection_size, apoc.coll.union(b1_categories, b2_categories) AS union_categories

//Construction de l'union
WITH b1, b2, (intersection_size)/(size(union_categories)) as jacard_similarity
WHERE jacard_similarity >0.5 AND b2.num_of_reviews >=10 AND b2.avg_rating > b1.avg_rating //0.5 et 10 sont des seuils abitraires
RETURN b1,  b2 AS best_b1_busisiness
ORDER BY b2.num_of_reviews DESC}

WITH b1,best_b1_busisiness
MATCH  (:User) -[r:REVIEWED]->(best_b1_busisiness)
WHERE r.text is not null and size(r.text) > 0 and r.rating is not null and r.rating > 3.5 //seuil arbitaire
WITH r,b1, best_b1_busisiness,point.distance(point({latitude: best_b1_busisiness.latitude, longitude: best_b1_busisiness.longitude}), point({latitude: b1.latitude, longitude: b1.longitude})) AS distance
RETURN r.rating ,r.text,best_b1_busisiness.url, best_b1_busisiness.hours,best_b1_busisiness.relative_results
ORDER BY distance DESC

```


Merci d'avoir lu jusqu'ici. Nous espérons que ce projet vous a été utile. N'hésitez pas à nous faire part de vos commentaires et suggestions.







